import peewee as pw

database = pw.SqliteDatabase("mydb.db")

class BaseModel(pw.Model):
    class Meta:
        database = database

class Image(BaseModel):
    time = pw.DateTimeField()
    filename = pw.CharField()
    mime_type = pw.CharField()

database.create_tables([Image])

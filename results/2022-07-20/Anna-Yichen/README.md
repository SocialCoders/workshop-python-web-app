# workshop-python-web-app

Project for a Workshop to build a picture server in python with flask and peewee.

# Linux und Mac

Führe aus im Terminal:

```bash

bash setup.sh

cd starting

gunicorn main:app

```

# Windows

Öffne CMD im workshop Ordner (Option unter Rechtsklick im Windows Explorer).

Dann gib ein:

```bat

setup.bat

cd starting

gunicorn main:app

```

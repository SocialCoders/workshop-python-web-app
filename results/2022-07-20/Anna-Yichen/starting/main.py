from flask import Flask, redirect, render_template, request
import db
import os 

app = Flask(__name__, static_folder="static", static_url_path="/static")

app.config["UPLOAD_FOLDER"] = "static"

user = db.User.create(name = "<YOUR HERE>")

@app.route("/")
def index():
    return render_template("index.html", users = db.User.select())

@app.route("/upload", methods=["POST"])
def upload():
    file = request.files["file"]
    content = file.read()

    with open(os.path.join(app.config["UPLOAD_FOLDER"], file.filename),"wb") as fh: 
        fh.write(content)
        return redirect("/static/" + file.filename)

    return content


if __name__ == '__main__':
    app.run()


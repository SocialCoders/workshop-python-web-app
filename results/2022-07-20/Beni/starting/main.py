from flask import Flask, render_template, request
import os
import db

app = Flask(__name__)

app.config["UPLOAD_FOLDER"] = os.path.join("static")

db.User.create(name = "Löwe", password = 123)

@app.route("/",methods = ["GET", "POST"])
def index(): 
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        user = db.User.get((db.User.name == username) & (db.User.password == password))

        file = request.files["file"] 

        file.save(os.path.join(app.config["UPLOAD_FOLDER"], file.filename))

    return render_template("index.html")


if __name__ == '__main__':
    app.run()

from flask import Flask, render_template, request, redirect, url_for
from Crypto.Hash import SHA3_256
from werkzeug.utils import secure_filename
from datetime import datetime
import db
import os

UPLOAD_FOLDER = "static/uploads"
ALLOWED_EXTENSIONS = {"jpg", "jpeg", "gif", "png" }

app = Flask(__name__, static_folder="static", static_url_path="/static")
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER

@app.route("/")
def index():
    return render_template("index.html", images = db.Image.select().order_by(db.Image.time))

def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/upload", methods=["GET", "POST"])
def upload():
    if request.method == "GET":
        return render_template("upload.html")
    else:
        if "file" not in request.files:
            return redirect(request.url)

        file = request.files["file"]

        if file.filename == '':
            return redirect(request.url)

        if file and allowed_file(file.filename):

            mime_type = file.filename.rsplit(".", 1)[1].lower()
            content = file.read()
            filename = SHA3_256.new(content).hexdigest()

            with open(os.path.join(app.config["UPLOAD_FOLDER"], filename + "." + mime_type), "wb") as fh:
                fh.write(content)

            db.Image.create(
                time=datetime.now(),
                filename=filename,
                mime_type=mime_type,
            )
            return redirect(url_for("index"))

@app.route("/delete", methods=["POST"])
def delete_image():
    if not allowed_file(request.form["filename"]):
        return redirect(url_for("index"))

    filename = request.form["filename"]
    image = db.Image.delete().where(db.Image.filename == filename).execute()
    os.remove(os.path.join(app.config["UPLOAD_FOLDER"], filename))

    return redirect(url_for("index"))

if __name__ == '__main__':
    app.run()

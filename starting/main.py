from flask import Flask, render_template
import db

app = Flask(__name__)

user = db.User.create(name = "<YOUR NAME HERE>")

@app.route("/")
def index():
    return render_template("index.html", users = db.User.select())

if __name__ == '__main__':
    app.run()

import peewee as pw

database = pw.SqliteDatabase("mydb.db")

class BaseModel(pw.Model):
    class Meta:
        database = database

class User(BaseModel):
    id = pw.AutoField()
    name = pw.CharField()

database.create_tables([User])
